﻿using DeadMosquito.AndroidGoodies;
using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PushManager : MonoBehaviour
{
    public String smsEN;
    public String smsRU;

    [Space] const String channelId = "TestChannelPush";
    const int notificationId = 93;
    const int SecondOfDay = 86400;
    private void Start()
    {
        if (AGNotificationManager.AreNotificationsEnabled && AGNotificationManager.AreNotificationChannelsSupported)
        {
            AGNotificationManager.DeleteNotificationChannel(channelId);
            CreateChannel();
            CreatePushSystem();
        }
    }

    [UsedImplicitly]
    public void CreateChannel()
    {
        var channel = new NotificationChannel(channelId, channelId, AGNotificationManager.Importance.Max);
        channel.VibrationPattern = new long[] { 0, 400, 1000, 600, 1000 };
        channel.BypassDnd = true;
        channel.ShowBadge = true;
        channel.EnableLights = true;
        channel.EnableVibration = true;
        channel.LockscreenVisibility = Notification.Visibility.Public;
        AGNotificationManager.CreateNotificationChannel(channel);
    }

    [UsedImplicitly]
    public void CreatePushSystem()
    {
        var builder = new Notification.Builder(channelId)
    .SetContentTitle("FoxyDoll")
    .SetSmallIcon("notify_icon_small");

        if (Application.systemLanguage == SystemLanguage.Russian || Application.systemLanguage == SystemLanguage.Ukrainian || Application.systemLanguage == SystemLanguage.Belarusian)
        {
            builder.SetContentText(smsRU);
        } else
        {
            builder.SetContentText(smsEN);
        }

        DateTime bufer = new DateTime(0);

        if (DateTime.Now.Hour < 18)
        {
            bufer = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
        } else
        {
            bufer = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1, 18, 0, 0);
        }

           var time = DateTime.Now.AddTicks(bufer.Ticks - DateTime.Now.Ticks); //is scheduled at 20 seconds after call

        const long intervalMillis = SecondOfDay * 1000L; // repeats every 10 seconds

        AGNotificationManager.NotifyRepeating(null, notificationId, builder.Build(), intervalMillis, time);
    }
}
