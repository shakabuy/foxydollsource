﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
public class Profiles : MonoBehaviour
{
    public ProfliesPaths pathsObj;
    public List<SavedProfile> profiles;
    public void LoadAllProfileInList()
    {
        for (int i = 0; i < pathsObj.Data.Paths.Count; i++)
        {
            SavedProfile bufer = SavedProfile.Load(pathsObj.Data.Paths[i]);
            profiles.Add(bufer);
        }
    }
}
