﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Editor_SubItem : MonoBehaviour
{
    public int id;
    private void Start()
    {
        Global.OnClickedSubCategory += MinimazeSubCategory;
    }
    public void OpenSubItem()
    {
        Global.Data.OnClickedSubCategoryData();
        // StartCoroutine(Global.Data.Editor.Open_SubCategroies(id));
        Global.Data.Editor.Open_SubCategroies(id);
        gameObject.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
    }
    public void MinimazeSubCategory()
    {
        gameObject.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
    }
    private void OnDestroy()
    {
        Global.OnClickedSubCategory -= MinimazeSubCategory;
    }
}
