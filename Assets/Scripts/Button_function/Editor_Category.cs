﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Editor_Category : MonoBehaviour
{
    public int id;
    private void Start()
    {
        Global.OnClickedCategory += MinimazeCategory;
    }
    public void OpenItem()
    {
        Global.Data.OnClickedCategoryData();
        gameObject.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
        Global.Data.Editor.Open_Categories(id);
    }
    public void MinimazeCategory()
    {
        gameObject.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
    }
    private void OnDestroy()
    {
        Global.OnClickedCategory -= MinimazeCategory;
    }
}
