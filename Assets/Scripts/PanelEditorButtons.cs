﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelEditorButtons : MonoBehaviour
{
    public GameObject panel_InputText;
    public GameObject panel_saveAccept;
    public Text textUI;

    public void Switch()
    {
        if (Global.Data.Editor.IsNewProfile() == true)
        {
            panel_saveAccept.SetActive(false);
            panel_InputText.SetActive(!panel_InputText.activeSelf);
        } else
        {
           StartCoroutine(Save());
        }
        
    }
    public void ToSave()
    {
        StartCoroutine(Save());
    }
    public IEnumerator Save()
    {
        SDKManager.ShowInterstitial(); //ADS

        yield return StartCoroutine(Global.Data.Editor.SaveProfile(textUI.text));
        Global.Data.MainMenu.UpdateList();
        Global.Data.LoadPanel_bufer.ChangeText(100);
        yield return new WaitForSeconds(0.1f);
        panel_InputText.SetActive(false);
        HomeInter();
        yield break;
    }
    public void ToHome()
    {       
        if(Global.Data.Editor.CheckCurrentAndTargetProfile() == false)
        {
            panel_saveAccept.SetActive(true);
        } else
        {//ADS
            Home();
        }
    }
    public void Accept()
    {
        if (Global.Data.Editor.IsNewProfile() == true)
        {
            Switch();
        }
        else
        {
            StartCoroutine(Save());
        }
    }
    public void Home()
    {
        SDKManager.ShowInterstitial();

        GlobalMainMenu.Data.RedactorPanel.SetActive(false);
        GlobalMainMenu.Data.MenuPanel.SetActive(true);
        if(Global.Data.LoadPanel_bufer != null)
        {
            Destroy(Global.Data.LoadPanel_bufer.gameObject);
        }
        //
        panel_InputText.SetActive(false);
        panel_saveAccept.SetActive(false);
    }
    public void HomeInter()
    {
        GlobalMainMenu.Data.RedactorPanel.SetActive(false);
        GlobalMainMenu.Data.MenuPanel.SetActive(true);
        if (Global.Data.LoadPanel_bufer != null)
        {
            Destroy(Global.Data.LoadPanel_bufer.gameObject);
        }
        //
        panel_InputText.SetActive(false);
        panel_saveAccept.SetActive(false);
    }
}
