﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.IO;
using System;

public class Editor : MonoBehaviour
{
    public Sprite template;
    public Items Asset_Items;

    [Space]
    public Transform parent_Template;
    public GameObject prefab_Template;

    [Space]
    public Transform parent_Category;
    public Transform parent_SubCategory;
    public Transform parent_Item;
    [Space]
    public GameObject prefab_Category;
    public GameObject prefab_SubCategory;
    public GameObject prefab_Item;
    public GameObject prefab_Background;


    [Space]
    [Space]
    public List<GameObject> buferList_Items;
    public List<GameObject> buferList_SubCategories;
    public List<ItemID> List_ItemsID;
    public List<TemplateID> List_TemplateID;
    [Space]
    public int id_Category, id_SubCategory, id_Profile;
    public GameObject prefabLayer;
    public List<GameObject> LayersParents;
    public void StartEditor()
    {
        for (int i = 0; i < Asset_Items.items.Count; i++)
        {
            GameObject bufer_Item = Instantiate(prefab_Category, parent_Category);
            bufer_Item.name = Asset_Items.items[i].name;
            bufer_Item.GetComponent<Image>().sprite = Asset_Items.items[i].Categories.image;
            SpriteState buferState = new SpriteState();
            buferState.selectedSprite = Asset_Items.items[i].Categories.imageoff;
            bufer_Item.GetComponent<Button>().spriteState = buferState;
            bufer_Item.GetComponent<Editor_Category>().id = i;
            if (i == 0)
            {
                bufer_Item.GetComponent<RectTransform>().localScale = new Vector3(1.25f, 1.25f, 1.25f);
            }
        }
        for (int i = 0; i < Asset_Items.LayoutsCount; i++)
        {
            GameObject bufer = Instantiate(prefabLayer, parent_Template);
            bufer.name = i.ToString();
            LayersParents.Add(bufer);
        }

        Open_Categories(0);      
    }
    public void Open_Categories(int id)
    {
        id_Category = id;

        for (int i = 0; i < buferList_SubCategories.Count; i++)
        {
            Destroy(buferList_SubCategories[i]);
        }
        buferList_SubCategories.Clear();

        for (int i = 0; i < Asset_Items.items[id_Category].Categories.SubCategories.Count; i++)
        {
            GameObject bufer_SubCategory = Instantiate(prefab_SubCategory, parent_SubCategory);
            bufer_SubCategory.name = Asset_Items.items[id_Category].Categories.name + "_" + Asset_Items.items[id_Category].Categories.SubCategories[i].name;
            bufer_SubCategory.GetComponent<Image>().sprite = Asset_Items.items[id_Category].Categories.SubCategories[i].image;
            SpriteState buferState = new SpriteState();
            buferState.selectedSprite = Asset_Items.items[id_Category].Categories.SubCategories[i].imageoff;
            bufer_SubCategory.GetComponent<Button>().spriteState = buferState;
            bufer_SubCategory.GetComponent<Editor_SubItem>().id = i;
            if (i == 0)
            {
                bufer_SubCategory.GetComponent<RectTransform>().localScale = new Vector3(1.25f, 1.25f, 1.25f);
            }

            buferList_SubCategories.Add(bufer_SubCategory);
        }
        if (id == 0 && id_Profile == -1)
        {
            //StartCoroutine(Open_SubCategroies(2)); 
            Open_SubCategroies(2);
        }
        else
        {
            // StartCoroutine(Open_SubCategroies(0));
            Open_SubCategroies(0);
        }
    }
    public /*IEnumerator*/  void Open_SubCategroies(int id)
    {
        id_SubCategory = id;
        Debug.Log("Open SubCategory - " + id);
        //Destroy list bufer
        for (int i = 0; i < buferList_Items.Count; i++)
        {
            Destroy(buferList_Items[i]);
        }
        buferList_Items.Clear();
        List_ItemsID.Clear();

        for (int i = 0; i < Asset_Items.items[id_Category].Categories.SubCategories[id_SubCategory].Items.Count; i++)
        {
            GameObject bufer_item = Instantiate(prefab_Item, parent_Item);
            bufer_item.name = Asset_Items.items[id_Category].Categories.name + "_" + Asset_Items.items[id_Category].Categories.SubCategories[id_SubCategory].name + "_"
                + Asset_Items.items[id_Category].Categories.name + "_" + Asset_Items.items[id_Category].Categories.SubCategories[id_SubCategory].Items[i].name;

            if (Asset_Items.items[id_Category].Categories.SubCategories[id_SubCategory].Items[i].icon == null)
            {
                bufer_item.GetComponent<Image>().sprite = Asset_Items.items[id_Category].Categories.SubCategories[id_SubCategory].Items[i].image;
            } else
            {
                bufer_item.GetComponent<Image>().sprite = Asset_Items.items[id_Category].Categories.SubCategories[id_SubCategory].Items[i].icon;
            }
            buferList_Items.Add(bufer_item);
            List_ItemsID.Add(bufer_item.GetComponent<ItemID>());

            bufer_item.GetComponent<ItemID>().SetIDs(id_Category, id_SubCategory, i);

            for (int k = 0; k < List_TemplateID.Count; k++)
            {
                if (List_TemplateID[k].ID_template.ID_Category == id_Category && List_TemplateID[k].ID_template.ID_SubCategory == id_SubCategory)
                {
                    if (List_TemplateID[k].ID_template.ID_Item == List_ItemsID[i].ID.ID_Item)
                    {
                        List_ItemsID[i].SwitchMark(true);
                        List_ItemsID[i].isUsed = true;
                    }
                }
            }
          //  yield return new WaitForEndOfFrame();
        }

    }
    public void Open_Item(int id_ct, int id_subct, int id_item)
    {
        for (int i = 0; i < List_ItemsID.Count; i++)
        {
            List_ItemsID[i].SwitchMark(false);
        }
        for (int i = 0; i < List_TemplateID.Count; i++)
        {
            if(List_TemplateID[i].ID_template.ID_Category == id_ct)
            {
                if(List_TemplateID[i].ID_template.ID_SubCategory == id_subct)
                {
                    if(List_TemplateID[i].ID_template.ID_Item == id_item)
                    {
                        if(Asset_Items.items[id_ct].Categories.SubCategories[id_subct].isOneTime)
                        {
                            Destroy(List_TemplateID[i].gameObject);
                            List_TemplateID.RemoveAt(i);
                            return;
                        }
                    }
                }
            }
        }
        for (int k = 0; k < List_TemplateID.Count; k++)
        {
            if (List_TemplateID[k].ID_template.ID_Category == id_ct && List_TemplateID[k].ID_template.ID_SubCategory == id_subct)
            {
                Destroy(List_TemplateID[k].gameObject);
                List_TemplateID.RemoveAt(k);
            }
        }
        GameObject bufer_item;
        if (Asset_Items.items[id_ct].Categories.SubCategories[id_subct].isBackground)
        {
            bufer_item = Instantiate(prefab_Background, LayersParents[Asset_Items.items[id_ct].Categories.SubCategories[id_subct].Items[id_item].layer_id].transform);
        }
        else
        {
            bufer_item = Instantiate(prefab_Template, LayersParents[Asset_Items.items[id_ct].Categories.SubCategories[id_subct].Items[id_item].layer_id].transform);
        }

        bufer_item.GetComponent<Image>().sprite = Asset_Items.items[id_ct].Categories.SubCategories[id_subct].Items[id_item].image;
        bufer_item.name = id_ct + "_" + id_subct + "_" + id_item;
       // Debug.Log(id_item);
        List_ItemsID[id_item].SwitchMark(true);



        bufer_item.GetComponent<TemplateID>().UpdateData(id_ct, id_subct, id_item);
        List_TemplateID.Add(bufer_item.GetComponent<TemplateID>());
    }
    public void AddDeafultItems()
    {
        for (int Cat = 0; Cat < Asset_Items.items.Count; Cat++)
        {
            for (int SubCat = 0; SubCat < Asset_Items.items[Cat].Categories.SubCategories.Count; SubCat++)
            {
                for (int id_item = 0; id_item < Asset_Items.items[Cat].Categories.SubCategories[SubCat].Items.Count; id_item++)
                {
                    if (Asset_Items.items[Cat].Categories.SubCategories[SubCat].Items[id_item].isDefault)
                    {
                        AddItem(Cat, SubCat, id_item);
                    }
                }
            }
        }
    }
    public void AddItem(int id_ct, int id_subct, int id_item)
    {
        GameObject bufer_item = Instantiate(prefab_Template, LayersParents[Asset_Items.items[id_ct].Categories.SubCategories[id_subct].Items[id_item].layer_id].transform);
        bufer_item.GetComponent<Image>().sprite = Asset_Items.items[id_ct].Categories.SubCategories[id_subct].Items[id_item].image;  
        bufer_item.name = id_ct + "_" + id_subct + "_" + id_item;       
        bufer_item.GetComponent<TemplateID>().UpdateData(id_ct, id_subct, id_item);
        List_TemplateID.Add(bufer_item.GetComponent<TemplateID>());
    }
    public void OpenProfile(int id)
    {
        id_Profile = id;
        if (List_TemplateID.Count != 0)
        {
            for (int i = 0; i < List_TemplateID.Count; i++)
            {
                if (List_TemplateID[i] != null)
                {
                    Destroy(List_TemplateID[i].gameObject);
                }
            }
        }

        List_TemplateID.Clear();

        if (id != -1)
        {
            for (int i = 0; i < Global.Data.Profiles_list.profiles[id].SavedItems.Count; i++)
            {
                SavedProfile.SavedItem bufer = Global.Data.Profiles_list.profiles[id].SavedItems[i];

                ///
                Open_Categories(bufer.ID_Category);
                Open_SubCategroies(bufer.ID_SubCategory);
                ///

                Open_Item(bufer.ID_Category, bufer.ID_SubCategory, bufer.ID_Item);
            }
        }
        else
        {           
            AddDeafultItems();
            Open_Categories(0);
        }
    }
    public IEnumerator SaveProfile (string name)
    {
        Global.Data.LoadPanel_bufer = Instantiate(Global.Data.LoadPanel_prefab, Global.Data.LoadPanel_parent).GetComponent<LoadPanel>();

        SavedProfile profile = new SavedProfile();
        Texture2D result = template.texture;
        if (id_Profile == -1)
        {
            Global.Data.LoadPanel_bufer.ChangeText(10);
            yield return new WaitForEndOfFrame();
            for (int i = 0; i < List_TemplateID.Count; i++)
            {
                profile.SavedItems.Add(List_TemplateID[i].ID_template);
            }

            profile.Name = name;
            profile.Path = ProfliesPaths.ProfilesPath + "Profile_" + (Global.Data.Profiles_list.profiles.Count + 1).ToString() + ".txt";

            Debug.Log(profile.Path);
            Global.Data.LoadPanel_bufer.ChangeText(30);
            yield return new WaitForEndOfFrame();
            #region CreateTexture

            for (int i = 0; i < Asset_Items.LayoutsCount; i++)
            {
                for (int k = 0; k < List_TemplateID.Count; k++)
                {
                    if (Asset_Items.items[List_TemplateID[k].ID_template.ID_Category].Categories.SubCategories[List_TemplateID[k].ID_template.ID_SubCategory].Items[List_TemplateID[k].ID_template.ID_Item].layer_id == i)
                    {
                        if (!Asset_Items.items[List_TemplateID[k].ID_template.ID_Category].Categories.SubCategories[List_TemplateID[k].ID_template.ID_SubCategory].isBackground)
                        {
                            result = result.AlphaBlend(Asset_Items.items[List_TemplateID[k].ID_template.ID_Category].Categories.SubCategories[List_TemplateID[k].ID_template.ID_SubCategory].Items[List_TemplateID[k].ID_template.ID_Item].image.texture);
                        } else
                        {
                            profile.isBackground = true;
                            profile.IDBackground = List_TemplateID[k].ID_template.ID_Item;
                        }
                    }
                }
            }
            Global.Data.LoadPanel_bufer.ChangeText(40);
            yield return new WaitForEndOfFrame();
            byte[] bytes = result.EncodeToPNG();
            string path = ProfliesPaths.ProfilesPath + "Image_" + (Global.Data.Profiles_list.profiles.Count + 1).ToString() + ".png";
            Global.Data.LoadPanel_bufer.ChangeText(50);
            yield return new WaitForEndOfFrame();

            File.WriteAllBytes(path, bytes);
            #endregion
            profile.Photo = result;
            profile.PathPhoto = path;
            Global.Data.LoadPanel_bufer.ChangeText(60);
            yield return new WaitForEndOfFrame();
            SavedProfile.Save(profile, id_Profile);
            Global.Data.LoadPanel_bufer.ChangeText(70);
            yield return new WaitForEndOfFrame();
        }
        else
        {
            Global.Data.LoadPanel_bufer.ChangeText(10);
            yield return new WaitForEndOfFrame();
            Global.Data.Profiles_list.profiles[id_Profile].SavedItems.Clear();

            for (int i = 0; i < List_TemplateID.Count; i++)
            {
                Global.Data.Profiles_list.profiles[id_Profile].SavedItems.Add(List_TemplateID[i].ID_template);
            }
            profile = Global.Data.Profiles_list.profiles[id_Profile];
            Global.Data.LoadPanel_bufer.ChangeText(30);
            yield return new WaitForEndOfFrame();
            #region CreateTexture
            for (int i = 0; i < Asset_Items.LayoutsCount; i++)
            {
                for (int k = 0; k < List_TemplateID.Count; k++)
                {
                    if (Asset_Items.items[List_TemplateID[k].ID_template.ID_Category].Categories.SubCategories[List_TemplateID[k].ID_template.ID_SubCategory].Items[List_TemplateID[k].ID_template.ID_Item].layer_id == i)
                    {
                        if (!Asset_Items.items[List_TemplateID[k].ID_template.ID_Category].Categories.SubCategories[List_TemplateID[k].ID_template.ID_SubCategory].isBackground)
                        {
                            result = result.AlphaBlend(Asset_Items.items[List_TemplateID[k].ID_template.ID_Category].Categories.SubCategories[List_TemplateID[k].ID_template.ID_SubCategory].Items[List_TemplateID[k].ID_template.ID_Item].image.texture);
                        } else
                        {
                            profile.isBackground = true;
                            profile.IDBackground = List_TemplateID[k].ID_template.ID_Item;
                        }
                    }
                }
            }
            Global.Data.LoadPanel_bufer.ChangeText(40);
            yield return new WaitForEndOfFrame();
            byte[] bytes = result.EncodeToPNG();
            string path = profile.PathPhoto;
            Global.Data.LoadPanel_bufer.ChangeText(50);
            yield return new WaitForEndOfFrame();
            if (File.Exists(path))
            {
                Debug.Log("delete");
                File.Delete(path);
            }
            File.WriteAllBytes(path, bytes);
            profile.Photo = result;
            #endregion
            Global.Data.LoadPanel_bufer.ChangeText(60);
            yield return new WaitForEndOfFrame();
            SavedProfile.Save(profile, id_Profile);
            Global.Data.LoadPanel_bufer.ChangeText(70);
            yield return new WaitForEndOfFrame();
        }
        Global.Data.LoadPanel_bufer.ChangeText(80);
        yield return new WaitForEndOfFrame();
        GlobalMainMenu.Data.ViewWindow.LoadProfilesInWindowView();
        Global.Data.LoadPanel_bufer.ChangeText(90);
        yield return new WaitForEndOfFrame();
        GC.Collect();
        yield break;
    }
    public bool CheckCurrentAndTargetProfile()
    {
        if (!IsNewProfile())
        {
            if (Global.Data.Profiles_list.profiles[id_Profile].SavedItems.Count == List_TemplateID.Count)
            {
                for (int i = 0; i < List_TemplateID.Count; i++)
                {

                    if (!CheckProfileItem(Global.Data.Profiles_list.profiles[id_Profile].SavedItems[i], List_TemplateID[i].ID_template))
                    {                       
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        else
        {
            return false;
        }
    }

    public bool IsNewProfile()
    {
        if (id_Profile == -1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckProfileItem(SavedProfile.SavedItem x, SavedProfile.SavedItem y)
    {       
        if (x.ID_Category == y.ID_Category && x.ID_SubCategory == y.ID_SubCategory && x.ID_Item == y.ID_Item)
        {
            return true;
        }
        return false;
    }
}

