﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoadPanel : MonoBehaviour
{
    public Text Full;
    private void Start()
    {
        ChangeText(0);
    }
    public void ChangeText(int count)
    {
        if (Full != null)
        {
            Full.text = count + "%";
        }
    }
}
