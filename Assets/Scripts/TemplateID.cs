﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemplateID : MonoBehaviour
{
    public SavedProfile.SavedItem ID_template;
    public void UpdateData(int a, int b, int c)
    {
        ID_template.ID_Category = a;
        ID_template.ID_SubCategory = b;
        ID_template.ID_Item = c;
    }
}
