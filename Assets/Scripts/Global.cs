﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour
{
    [System.Serializable]
    public class Category
    {
        [System.Serializable]
        public class SubCategory
        {
            [System.Serializable]
            public class Item
            {
                public string name;
                public Sprite image;
                public Sprite icon;
                public int layer_id;

                [Space]
                
                [Space]
                public bool isDefault;
            }
            public string name;
            public Sprite image;
            public Sprite imageoff;
            [Space]
            public bool isOneTime;
            public bool isBackground;
            [Space]
            public List<Item> Items;
        }
        public string name;
        public Sprite image;
        public Sprite imageoff;
        public List<SubCategory> SubCategories;

        
    }

    [System.Serializable]
    public class Info
    {
        public string AppName;
        public Menu_Main MainMenu;
        public Profiles Profiles_list;
        public Editor Editor;
        public View View;

        [Space]
        public GameObject LoadPanel_prefab;
        public Transform LoadPanel_parent;
        [Space]
        public LoadPanel LoadPanel_bufer;
        public void OnClickedCategoryData()
        {
            OnClickedCategory();
        }
        public void OnClickedSubCategoryData()
        {
            OnClickedSubCategory();
        }
    }
    public delegate void ClickCategory();
    public static event ClickCategory OnClickedCategory;

    public delegate void ClickSubCategory();
    public static event ClickSubCategory OnClickedSubCategory;
    public static Info Data;
    public Info data_bufer;
   
    public void StartGlobal()
    {
        Data = data_bufer;
        
    }
}
