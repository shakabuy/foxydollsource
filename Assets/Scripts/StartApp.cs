﻿using DeadMosquito.AndroidGoodies;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Android;

public class StartApp : MonoBehaviour
{
    public Global glob;
    public GlobalMainMenu globMenu;
    private void Awake()
    {
        Application.targetFrameRate = 60;
        OnRequestPermissions();
    }
    private void Start()
    {
        StartCoroutine(StartGame());
    }
    public IEnumerator StartGame()
    {
        
        /* if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead) || !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
         {
             Permission.RequestUserPermission(Permission.ExternalStorageRead);
             Permission.RequestUserPermission(Permission.ExternalStorageWrite);
         }*/
        
        //Update Global
        globMenu.StartGlobalMenu();
        glob.StartGlobal();
        Global.Data.LoadPanel_bufer = Instantiate(Global.Data.LoadPanel_prefab, Global.Data.LoadPanel_parent).GetComponent<LoadPanel>();
        yield return new WaitForEndOfFrame();
        Global.Data.LoadPanel_bufer.ChangeText(20);
        yield return new WaitForEndOfFrame();
        Global.Data.Profiles_list.pathsObj.LoadPathsInStart(); //Paths Manager
        Global.Data.LoadPanel_bufer.ChangeText(47);
        yield return new WaitForEndOfFrame();
        Global.Data.Profiles_list.LoadAllProfileInList();      //Add profiles in list
        Global.Data.LoadPanel_bufer.ChangeText(64);
        yield return new WaitForEndOfFrame();
        GlobalMainMenu.Data.ViewWindow.LoadProfilesInWindowView();                   //Load last profile in view window
        Global.Data.LoadPanel_bufer.ChangeText(84);
        yield return new WaitForEndOfFrame();
        Global.Data.MainMenu.AddProfliesInListStartMenu();     //Add profiles in list start menu 
        Global.Data.LoadPanel_bufer.ChangeText(100);
        yield return new WaitForEndOfFrame();
        if (Global.Data.LoadPanel_bufer != null)
        {
            Destroy(Global.Data.LoadPanel_bufer.gameObject);
        }      
        Global.Data.Editor.StartEditor();
        GC.Collect();
        yield break;
    }
    public void OnRequestPermissions()
    {
        // Don't forget to also add the permissions you need to manifest!
        var permissions = new[]
        {
        AGPermissions.WRITE_EXTERNAL_STORAGE,
        AGPermissions.READ_EXTERNAL_STORAGE
    };

        // Filter permissions so we don't request already granted permissions,
        // otherwise if the user denies already granted permission the app will be killed
        var nonGrantedPermissions = permissions.ToList().Where(x => !AGPermissions.IsPermissionGranted(x)).ToArray();

        if (nonGrantedPermissions.Length == 0)
        {
            Debug.Log("User already granted all these permissions: " + string.Join(",", permissions));
            return;
        }

        // Finally request permissions user has not granted yet and log the results
        AGPermissions.RequestPermissions(permissions, results =>
        {
            // Process results of requested permissions
            foreach (var result in results)
            {
                Debug.Log(string.Format("Permission [{0}] is [{1}], should show explanation?: {2}",
                    result.Permission, result.Status, result.ShouldShowRequestPermissionRationale));
                if (result.Status == AGPermissions.PermissionStatus.Denied)
                {
                    // User denied permission, now we need to find out if he clicked "Do not show again" checkbox
                    if (result.ShouldShowRequestPermissionRationale)
                    {
                        // User just denied permission, we can show explanation here and request permissions again
                        // or send user to settings to do so
                    }
                    else
                    {
                        // User checked "Do not show again" checkbox or permission can't be granted.
                        // We should continue with this permission denied
                    }
                }
            }
        });
    }

}
