﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Items : ScriptableObject
{
    public int LayoutsCount;
   // public List<Global.Category> items = new List<Global.Category>();
    public List<Item> items = new List<Item>();
}
