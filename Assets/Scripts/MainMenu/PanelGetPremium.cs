﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelGetPremium : MonoBehaviour
{
    public GameObject Panel_GetPremium;

    public void SwitchPanel()
    {
        Panel_GetPremium.SetActive(!Panel_GetPremium.activeSelf);
    }
}
