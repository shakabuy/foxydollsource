﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
public class Menu_Main : MonoBehaviour
{
    public Item BgList;
    [Space]
    public GameObject prefab_profile;
    public Transform parent_profiles;

    public List<GameObject> profiles; 
    [HideInInspector]
    public int count;
    
    public void AddProfliesInListStartMenu()
    {
        count = Global.Data.Profiles_list.profiles.Count;

        for (int i = 0; i < count; i++)
        {
            GameObject bufer_profile = Instantiate(prefab_profile, parent_profiles);
            Global.Data.Profiles_list.profiles[i].LoadPhoto();
            bufer_profile.GetComponent<Profile>().UpdateData(i, Global.Data.Profiles_list.profiles[i].Name, Global.Data.Profiles_list.profiles[i].isBackground, Global.Data.Profiles_list.profiles[i].IDBackground, ImageHelpers.ToSprite(Global.Data.Profiles_list.profiles[i].Photo));

            profiles.Add(bufer_profile);
        }
    }
    public void UpdateList()
    {
        if (profiles.Count != 0)
        {
            for (int i = 0; i < profiles.Count; i++)
            {
                if (profiles[i] != null)
                {
                    Destroy(profiles[i]);
                }
            }
        }
        profiles.Clear();

        count = Global.Data.Profiles_list.profiles.Count;

        for (int i = 0; i < count; i++)
        {
            GameObject bufer_profile = Instantiate(prefab_profile, parent_profiles);
            bufer_profile.GetComponent<Profile>().UpdateData(i, Global.Data.Profiles_list.profiles[i].Name, Global.Data.Profiles_list.profiles[i].isBackground, Global.Data.Profiles_list.profiles[i].IDBackground, ImageHelpers.ToSprite(Global.Data.Profiles_list.profiles[i].Photo));
            profiles.Add(bufer_profile);
        }
    }

    public void DeleteProfile(int id)
    {
        if (File.Exists(Global.Data.Profiles_list.profiles[id].PathPhoto))
        {
            Debug.Log("delete");
            File.Delete(Global.Data.Profiles_list.profiles[id].PathPhoto);
        }
        
        Destroy(profiles[id]);
        File.Delete(Global.Data.Profiles_list.profiles[id].Path);

        Global.Data.Profiles_list.profiles.RemoveAt(id);
        Global.Data.Profiles_list.pathsObj.Data.Paths.RemoveAt(id);
        if (id == Global.Data.Profiles_list.pathsObj.Data.Last_ID)
        {
            Global.Data.Profiles_list.pathsObj.Save(0);
        } else
        {
            int bufer = Global.Data.Editor.id_Profile;
            if (bufer == -1)
            {
                Global.Data.Profiles_list.pathsObj.Save(Global.Data.Profiles_list.profiles.Count - 1);
            } else
            {
                Global.Data.Profiles_list.pathsObj.Save(bufer);
            }
        }
        UpdateList();
    }
}
