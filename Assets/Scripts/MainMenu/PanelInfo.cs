﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelInfo : MonoBehaviour
{
    public GameObject panel_info;
    public string url_youtube;
    public string url_inst;
    public string url_site;

    public void SwitchPanel()
    {
        panel_info.SetActive(!panel_info.activeSelf);
    }
}
