﻿using DeadMosquito.AndroidGoodies;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View : MonoBehaviour
{
    public GameObject PanelViewMain;
    public int current_id;
    public Image img;
    public Text nameText;
    public Image Background;

    [Space]
    public GameObject PanelViewSecond;
    public int current_idS;
    public Image imgS;
    public Text nameTextS;
    public Image BackgroundS;

    [Space]
    public GameObject panel_Delete;
    public int IDbufer;
    public void LoadProfilesInWindowView()
    {
        Debug.Log(Global.Data.Profiles_list.pathsObj.Data.Paths.Count);
        if (Global.Data.Profiles_list.pathsObj.Data.Paths.Count != 0)
        {
            current_id = Global.Data.Profiles_list.pathsObj.Data.Paths.Count - 1;
            Global.Data.Profiles_list.profiles[current_id].LoadPhoto();
            UpdateData(current_id, Global.Data.Profiles_list.profiles[current_id].Name, Global.Data.Profiles_list.profiles[current_id].isBackground, Global.Data.Profiles_list.profiles[current_id].IDBackground, ImageHelpers.ToSprite(Global.Data.Profiles_list.profiles[current_id].Photo));
        }
        else
        {
            PanelViewMain.SetActive(false);
        }
    }
    public void UpdateProfileInWindowView(Texture2D _result)
    {
        current_id = Global.Data.Profiles_list.pathsObj.Data.Paths.Count - 1;
        Global.Data.Profiles_list.profiles[current_id].Photo = _result;
        UpdateData(current_id, Global.Data.Profiles_list.profiles[current_id].Name, Global.Data.Profiles_list.profiles[current_id].isBackground, Global.Data.Profiles_list.profiles[current_id].IDBackground, ImageHelpers.ToSprite(Global.Data.Profiles_list.profiles[current_id].Photo));
    }
    public void UpdateData(int current_id_s, string name_s, bool isBackground, int idBG, Sprite img_s)
    {
        if (isBackground)
        {
            Background.sprite = Global.Data.MainMenu.BgList.Categories.SubCategories[0].Items[idBG].image;
        }
        else
        {
            Background.sprite = Global.Data.Editor.template;
        }

        current_id = current_id_s;
        IDbufer = current_id;
        nameText.text = name_s;
        img.sprite = img_s;
        PanelViewMain.SetActive(true);
    }
    public void UpdateDataSecond(int current_id_s, string name_s, bool isBackground, int idBG, Sprite img_s)
    {
        if (isBackground)
        {
            BackgroundS.sprite = Global.Data.MainMenu.BgList.Categories.SubCategories[0].Items[idBG].image;
        }
        else
        {
            BackgroundS.sprite = Global.Data.Editor.template;
        }

        current_idS = current_id_s;
        IDbufer = current_id_s;
        nameTextS.text = name_s;
        imgS.sprite = img_s;
        PanelViewSecond.SetActive(true);
    }
    public void Edit(bool isMain)
    {
        if (isMain)
        {
            IDbufer = current_id;
        }
        else
        {
            IDbufer = current_idS;
        }
        GlobalMainMenu.Data.RedactorPanel.SetActive(true);
        GlobalMainMenu.Data.MenuPanel.SetActive(false);
        Global.Data.Editor.OpenProfile(IDbufer);
        PanelViewSecondClose();
    }
    public void Delete(bool isMain)
    {
        if (isMain)
        {
            IDbufer = current_id;
        }
        else
        {
            IDbufer = current_idS;
        }
        panel_Delete.SetActive(!panel_Delete.activeSelf);
    }
    public void DeleteData()
    {        
        Global.Data.MainMenu.DeleteProfile(IDbufer);
        panel_Delete.SetActive(false);      
        if (Global.Data.Profiles_list.pathsObj.Data.Paths.Count != 0)
        {
            IDbufer = Global.Data.Profiles_list.pathsObj.Data.Paths.Count - 1;
            Global.Data.Profiles_list.profiles[IDbufer].LoadPhoto();
            UpdateData(IDbufer, Global.Data.Profiles_list.profiles[IDbufer].Name, Global.Data.Profiles_list.profiles[IDbufer].isBackground, Global.Data.Profiles_list.profiles[IDbufer].IDBackground, ImageHelpers.ToSprite(Global.Data.Profiles_list.profiles[IDbufer].Photo));
        }
        else
        {
            PanelViewMain.SetActive(false);
            
        }
        PanelViewSecond.SetActive(false);
    }
    [UsedImplicitly]
    public void Share()
    {
        
        AGShare.ShareTextWithImage("My subject", "Developed in the FoxyDoll", img.sprite.texture);
    }
    public void Download()
    {
        StartCoroutine(onDownload());
    }
    public IEnumerator onDownload()
    {
        Global.Data.LoadPanel_bufer = Instantiate(Global.Data.LoadPanel_prefab, Global.Data.LoadPanel_parent).GetComponent<LoadPanel>();
        yield return new WaitForEndOfFrame();
        Global.Data.LoadPanel_bufer.ChangeText(32);
        yield return new WaitForEndOfFrame();
        Global.Data.LoadPanel_bufer.ChangeText(44);
        yield return new WaitForEndOfFrame();
        Global.Data.LoadPanel_bufer.ChangeText(67);
        yield return new WaitForEndOfFrame();

        if (Global.Data.Profiles_list.profiles[current_id].isBackground)
        {
            Texture2D bufer = Global.Data.MainMenu.BgList.Categories.SubCategories[0].Items[Global.Data.Profiles_list.profiles[current_id].IDBackground].image.texture;
            bufer = bufer.AlphaBlend(img.sprite.texture);

            yield return AGFileUtils.SaveImageToGallery(bufer, nameText.text + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), Global.Data.AppName);
        }
        else
        {
            yield return AGFileUtils.SaveImageToGallery(img.sprite.texture, nameText.text + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), Global.Data.AppName);
        }
        Global.Data.LoadPanel_bufer.ChangeText(100);
        if (Global.Data.LoadPanel_bufer != null)
        {
            Destroy(Global.Data.LoadPanel_bufer.gameObject);
        }
        yield break;
    }
    public void PanelViewSecondClose()
    {
        PanelViewSecond.SetActive(false);
    }
}
