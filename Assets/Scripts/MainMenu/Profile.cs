﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Profile : MonoBehaviour
{
    public int ID;
    public string Name;
    public Image Background;
    public Image Photo;
    public Text name_ui;
    public Sprite Img;
    public bool isBG;
    public int IDBackground;
    public void UpdateData(int id, string name_s, bool isBackground, int idBG, Sprite img = null)
    {
        IDBackground = idBG;
        isBG = isBackground;

        if (isBackground)
        {
            Background.sprite = Global.Data.MainMenu.BgList.Categories.SubCategories[0].Items[idBG].image;
        } else
        {
            Background.sprite = Global.Data.Editor.template;
        }
        ID = id;
        Name = name_s;
        

        //UI
        name_ui.text = Name;
        if (img != null)
        {
            Img = img;
            Photo.sprite = Img;
        }

    }
    public void OpenPanel()
    {
        if (Img != null)
        {
            Global.Data.View.UpdateDataSecond(ID, Name, isBG, IDBackground, Img);
        } else
        {
            Global.Data.View.UpdateDataSecond(ID, Name,isBG, IDBackground, GlobalMainMenu.Data.Sprite_NotFound);
        }
    }
    public void SetImage(Sprite _img)
    {
        Img = _img;
        Photo.sprite = Img;
    }
}
