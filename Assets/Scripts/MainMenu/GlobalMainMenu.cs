﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalMainMenu : MonoBehaviour
{
    [System.Serializable]
    public class Info
    {
        public Sprite Sprite_NotFound;

        public GameObject MenuPanel;
        public GameObject RedactorPanel;
        public View ViewWindow;
    }
    public static  Info Data;
    public Info data_bufer;
    public void StartGlobalMenu()
    {
        Data = data_bufer;
    }
}
