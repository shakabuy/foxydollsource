﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNewProfile : MonoBehaviour
{
   public void Create()
    {
        GlobalMainMenu.Data.RedactorPanel.SetActive(true);
        GlobalMainMenu.Data.MenuPanel.SetActive(false);
        Global.Data.Editor.OpenProfile(-1);
    }
}
