﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.Xml;
using System.IO;


public class ProfliesPaths : MonoBehaviour
{
    [System.Serializable]
    [XmlRoot("ProfliesPaths")]
    public class ProfliesPathsClass
    {
        [XmlElement("lastID")]
        public int Last_ID;
        [XmlElement("liststring")]
        public List<string> Paths;
    }
    [XmlIgnore]
    public static string ProfilesPath;
    [XmlIgnore]
    public string privatePath;

    public ProfliesPathsClass Data = new ProfliesPathsClass();


    public void LoadPathsInStart()
    {
        Directory.CreateDirectory(Application.persistentDataPath + @"/Profiles");
        ProfilesPath = Application.persistentDataPath + @"/Profiles/";
        privatePath = ProfilesPath + "paths.txt";
        Debug.Log(Application.persistentDataPath);
        Load();
    }
    [ContextMenu("Save")]
    public void Save(int id_lastProfile = 0)
    {
        Debug.Log("Save to " + privatePath);
        XmlSerializer serializer = new XmlSerializer(typeof(ProfliesPathsClass));
        using (var stream = new FileStream(privatePath, FileMode.Create, FileAccess.Write))
        {
            ProfliesPathsClass bufer = new ProfliesPathsClass();
            bufer.Last_ID = id_lastProfile;
            bufer.Paths = Data.Paths;
            serializer.Serialize(stream, bufer);
        }        
    }


    public void Load()
    {
        if (File.Exists(privatePath))
        {
            StreamReader reader = new StreamReader(privatePath);
            XmlSerializer serializer = new XmlSerializer(typeof(ProfliesPathsClass));
            ProfliesPathsClass result = serializer.Deserialize(reader) as ProfliesPathsClass;
            Data.Last_ID = result.Last_ID;
            Data.Paths = result.Paths;
        }
    }
}
