﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml.Serialization;
using System.Xml;

[System.Serializable]
[XmlRoot("SavedProfile")]
public class SavedProfile
{
    [XmlElement("Name")]
    public string Name;
    [XmlElement("Path")]
    public string Path;
    [XmlElement("PathPhoto")]
    public string PathPhoto;
    [XmlElement("isBackground")]
    public bool isBackground;
    [XmlElement("IDBackground")]
    public int IDBackground = 0;

    [XmlIgnore]
    public Texture2D Photo;

    public List<SavedItem> SavedItems = new List<SavedItem>();
    [System.Serializable]
    public class SavedItem
    {
        public int ID_Category;
        public int ID_SubCategory;
        public int ID_Item;     
    }
    public void LoadPhoto()
    {
        Texture2D tex;
        //Debug.Log("load");
        byte[] fileData;
        fileData = File.ReadAllBytes(PathPhoto);
        tex = new Texture2D(2, 2);
        tex.LoadImage(fileData);
        Photo = tex;
    }
    
    public static SavedProfile Load(string path_s)
    {
        StreamReader reader = new StreamReader(path_s);

        XmlSerializer serializer = new XmlSerializer(typeof(SavedProfile));       

        SavedProfile result = serializer.Deserialize(reader) as SavedProfile;
        reader.Close();
        return result;
    }
    public static void Save(SavedProfile _profile, int _IDProfile)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(SavedProfile));
        if (_IDProfile == -1)
        {
            using (var stream = new FileStream(_profile.Path, FileMode.Create, FileAccess.Write))
            {
                serializer.Serialize(stream, _profile);
            }
            Global.Data.Profiles_list.profiles.Add(_profile);
            Global.Data.Profiles_list.pathsObj.Data.Paths.Add(_profile.Path);
            Global.Data.Profiles_list.pathsObj.Save(Global.Data.Profiles_list.profiles.Count-1);
        } else
        {
            using (var stream = new FileStream(_profile.Path, FileMode.Create, FileAccess.Write))
            {
                serializer.Serialize(stream, _profile);
            }
            Global.Data.Profiles_list.pathsObj.Save(_IDProfile);
        }

    }
}
