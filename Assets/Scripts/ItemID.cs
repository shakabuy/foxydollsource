﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemID : MonoBehaviour
{
    public GameObject CheckMark;
    public SavedProfile.SavedItem ID = new SavedProfile.SavedItem();
    public bool isUsed;
    public void SetIDs(int a, int b, int c)
    {
        ID.ID_Category = a;
        ID.ID_SubCategory = b;
        ID.ID_Item = c;
    }
    public void OpenItem()
    {
        Global.Data.Editor.Open_Item(ID.ID_Category, ID.ID_SubCategory, ID.ID_Item);
    }
    public void SwitchMark(bool active)
    {
        CheckMark.SetActive(active);
    }

    public void CheckMarkOff()
    {
        CheckMark.SetActive(false);
    }
    public void CheckMarkOn()
    {
        CheckMark.SetActive(true);
    }
}
