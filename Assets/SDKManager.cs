﻿using Facebook.Unity;
using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SDKManager : MonoBehaviour
{
    [Header("Google Ads")]
    public string AppID = "ca-app-pub-6188745395041430~7996147230";
    public string RequestBannerID = "ca-app-pub-6188745395041430/2783349003";
    public string RequestInterstitialID = "ca-app-pub-6188745395041430/5916778806";
    [Space]
    //public string TestDeviceID = "35CD5FC8842EC2FBBBC349B4ACA46FD5";

    private BannerView bannerView;
    private InterstitialAd interstitial;
    public static SDKManager Data;
    private void Awake()
    {
        Data = this;
    }
    private void Start()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            FB.Init(onInitComplete: ActivateApp);
        }


        //MobileAds
        MobileAds.Initialize(AppID);

        //ABS
        RequestBanner();
        RequestInterstitial();
    }
    public void ActivateApp()
    {
        FB.ActivateApp();
    }
    private void RequestBanner()
    {
        bannerView = new BannerView(RequestBannerID, AdSize.Banner, AdPosition.Bottom);

        bannerView.OnAdFailedToLoad += BannerHandleOnAdFailedToLoad;
        bannerView.OnAdLoaded += BannerHandleOnAdLoaded;
        bannerView.OnAdClosed += BannerHandleOnAdClosed;
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
    }
    private void RequestInterstitial()
    {
        interstitial = new InterstitialAd(RequestInterstitialID);

        interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        interstitial.OnAdClosed += HandleOnAdClosed;

        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
    }

    public static void ShowInterstitial()
    {
        if (Data.interstitial.IsLoaded())
        {
            Data.interstitial.Show();
        }
    }
    #region Banner
    public void BannerHandleOnAdLoaded(object sender, EventArgs args)
    {
        bannerView.Show();    
    }

    public void BannerHandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        RequestBanner();
    }

    public void BannerHandleOnAdClosed(object sender, EventArgs args)
    {
        RequestBanner();
    }  
    #endregion
#region Interstitial
    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        RequestInterstitial();
    }
    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        RequestInterstitial();
    }
    #endregion
    private void OnDestroy()
    {
        bannerView.OnAdFailedToLoad -= BannerHandleOnAdFailedToLoad;
        bannerView.OnAdLoaded -= BannerHandleOnAdLoaded;
        bannerView.OnAdClosed -= BannerHandleOnAdClosed;

        interstitial.OnAdFailedToLoad -= HandleOnAdFailedToLoad;
        interstitial.OnAdClosed -= HandleOnAdClosed;
    }
}
